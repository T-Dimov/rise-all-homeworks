using Day3TeamTasks;

namespace Day3TeamTasksTests
{
    [TestClass]
    public class FirstTaskTests
    {
        [TestMethod]
        public void HasSquareWithFibonacciNumbersReturnsTrueForMatrixWithFibonacciSquare()
        {
            int[,] matrix = new int[,]
            {
                {1, 16, 3, 5},
                {9, 13, 4, 12},
                {55, 3, 5, 0},
                {377, 13, 8,92}
            };

            bool result = FirstTask.HasSquareWithFibonacciNumbers(matrix);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void HasSquareWithFibonacciNumbersReturnsFalseForMatrixWithoutFibonacciSquare()
        {
            int[,] matrix = new int[,]
            {
                {2, 4, 6},
                {8, 9, 1},
                {2, 7, 7}
            };

            bool result = FirstTask.HasSquareWithFibonacciNumbers(matrix);

            Assert.IsFalse(result);
        }
    }
}