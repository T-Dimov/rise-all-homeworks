﻿using Day5Tasks;

namespace Day5TasksTests
{
    [TestClass]
    public class SecondTaskTests
    {
        [TestMethod]
        public void EraseMiddleElementShouldWorkProperlyWithOddCountIntegers()
        {
            var array = new int[] { 1, 9, 4, 9, 2, 4, 7 };
            var numbers = new LinkedList<int>(array);

            var expected = new LinkedList<int>(new[] { 1, 9, 4, 2, 4, 7 });
            
            SecondTask.EraseMiddleElement(numbers);

            CollectionAssert.AreEqual(expected, numbers);
        }

        [TestMethod]
        public void EraseMiddleElementShouldWorkProperlyWithEvenCountIntegers()
        {
            var array = new int[] { 1, 9, 4, 9, 2, 4, 7, 2};
            var numbers = new LinkedList<int>(array);

            var expected = new LinkedList<int>(new[] { 1, 9, 4, 9, 4, 7, 2 });

            SecondTask.EraseMiddleElement(numbers);

            CollectionAssert.AreEqual(expected, numbers);
        }

        [TestMethod]
        public void EraseMiddleElementShouldWorkProperlyWithStrings()
        {
            var array = new string[] { "Monday", "Tuesday", "Wednesday"};
            var words = new LinkedList<string>(array);

            var expected = new LinkedList<string>(new[] { "Monday", "Wednesday"});

            SecondTask.EraseMiddleElement(words);

            CollectionAssert.AreEqual(expected, words);
        }
    }
}
