using Day5Tasks;

namespace Day4TasksTests
{
    [TestClass]
    public class FirstTaskTests
    {
        [TestMethod]
        public void GetUniqueElementsShouldWorkProperlyWithNumbers()
        {
            var numbers = new List<int> { 8, 19, 29, 11, 23, 11, 90, 87, 88, 90, 90, 8 };

            var expected = new List<int> { 8, 19, 29, 11, 23, 90, 87, 88 };
            var actual = FirstTask.GetUniqueElements(numbers);

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetUniqueElementsShouldWorkProperlyWithQniqueNumbers()
        {
            var numbers = new List<int> { 8, 19, 29, 11, 23, 11, 90, 87, 88};

            var result = FirstTask.GetUniqueElements(numbers);

            CollectionAssert.AllItemsAreUnique(result);
        }

        [TestMethod]
        public void GetUniqueElementsShouldWorkProperlyWithStrings()
        {
            var strings = new List<string> {"Monday", "Tuesday", "Sunday", "Monday" };

            var result = FirstTask.GetUniqueElements(strings);

            CollectionAssert.AllItemsAreUnique(result);
        }

        [TestMethod]
        public void GetUniqueElementsShouldWorkProperlyWithObjectsWithSameNameAndAge() 
        {
            // Assuming two dogs with same name and age are equal

            var dog = new Dog("Pesho", 15);
            var dog2 = new Dog("Pesho", 15);

            var dogs = new List<Dog> { dog, dog2 };

            var expectedCount = 1;
            var actualCount = FirstTask.GetUniqueElements(dogs).Count;

            Assert.AreEqual(expectedCount, actualCount);
        }
    }
}