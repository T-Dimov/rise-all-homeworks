﻿using Day5Tasks;

namespace Day5TasksTests
{
    [TestClass]
    public class FourthTaskTests
    {
        [TestMethod]
        public void GetExpandedExpressionShouldSucceed() 
        {
            var expression = "AB3(DC)2(F)2(E3(G))";

            var expected = "ABDCDCDCFFEGGGEGGG";
            var actual = FourthTask.GetExpandedExpression(expression);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetExpandedExpressionShouldSucceedWithSimpleEpression()
        {
            var expression = "ABC";

            var expected = "ABC";
            var actual = FourthTask.GetExpandedExpression(expression);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestExpressionExpansion()
        {
            //Assert
            string expression = "AB3(DC)2(F)2(E3(G))";
            string expected = "ABDCDCDCFFEGGGEGGG";

            //Act
            string result = FourthTask.GetExpandedExpression(expression);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestExpressionExpansionWithNumberFollowedByLetters()
        {
            //Assert
            string expression = "3A1B3(DC)2(0F)2(E3(G))";
            string expected = "AAABDCDCDCFFEGGGEGGG";

            //Act
            string result = FourthTask.GetExpandedExpression(expression);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestExpressionExpansionWitZerosInBrackets()
        {
            //Assert
            string expression = "AB3(DC)2(0FZZ0R0P)2(E3(G))";
            string expected = "ABDCDCDC0FZZ0R0P0FZZ0R0PEGGGEGGG";

            //Act
            string result = FourthTask.GetExpandedExpression(expression);

            //Assert
            Assert.AreEqual(expected, result);
        }
    }
}
