﻿namespace Day5Tasks
{
    public class SecondTask
    {
        // Erase the middle element of a given Linked list

        public static void EraseMiddleElement<T>(LinkedList<T> elements) 
        {
            var count = elements.Count / 2;

            var elementToDelete = elements.First;

            for (int i = 0; i < count; i++)
            {
                elementToDelete = elementToDelete.Next;
            }

            elements.Remove(elementToDelete);
        }
    }
}
