namespace Day5Tasks
{
    public class FourthTask
    {
        // Implement expression expansion
        //For example the expression AB3(DC)2(F)2(E3(G)) is expanded to ABDCDCDCFFEGGGEGGG

        public static string GetExpandedExpression(string expression)
        {
            var stack = new Stack<char>();

            foreach (var c in expression)
            {
                stack.Push(c);
            }

            stack.Reverse();

            var result = new Stack<char>();

            while (stack.Any())
            {
                var current = stack.Pop();

                if (current == '(')
                {
                    var count = int.Parse(stack.Pop().ToString());

                    var currentExpression = "";

                    while (result.Peek() != ')')
                    {
                        currentExpression += result.Pop();
                    }

                    var multipliedExpression = "";

                    for (int i = 0; i < count; i++)
                    {
                        multipliedExpression += currentExpression;
                    }

                    result.Pop();

                    for (int i = multipliedExpression.Length - 1; i >= 0; i--)
                    {
                        result.Push(multipliedExpression[i]);
                    }
                }

                if (current != '(')
                {
                    result.Push(current);
                }
            }

            var expandedExpression = "";

            while (result.Any())
            {
                expandedExpression += result.Pop();
            }

            return expandedExpression;
        }
    }
}
