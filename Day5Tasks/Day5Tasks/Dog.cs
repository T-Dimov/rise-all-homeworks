﻿using System.Security.Principal;

namespace Day5Tasks
{
    public class Dog 
    {
        public Dog(string name, int age)
        {
            this.Name = name;
            this.Age = age;
        }

        public string Name { get; set; }

        public int Age { get; set; }

        public override bool Equals(object? obj)
        {
            var other = obj as Dog;

            return this.Name == other?.Name && this.Age == other.Age; 
        }

        public override int GetHashCode()
        {
            return System.HashCode.Combine(Name, Age);
        }
    }
}
