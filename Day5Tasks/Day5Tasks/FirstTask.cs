﻿namespace Day5Tasks
{
    public class FirstTask 
    {
        // Given a list return a new list containing only the unique elements of the original.

        public static List<T> GetUniqueElements<T>(List<T> elements) 
        {
            var result = new HashSet<T>();

            foreach (var element in elements) 
            {
                result.Add(element);
            }

            return result.ToList();
        }
    }
}
