﻿using Day4Tasks;
using System;

namespace Day4TasksTests
{
    [TestClass]
    public class FindRootOfThreeTests
    {
        [TestMethod]
        public void TestGetRootOfThreeShouldReturnCorrectResultWithOne()
        {
            var number = 1;

            var expected = 1;
            var actual = FindRootOfThree.GetRootOfThree(number);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGetRootOfThreeShouldReturnCorrectResultWithZero()
        {
            var number = 0;

            var expected = 0;
            var actual = FindRootOfThree.GetRootOfThree(number);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGetRootOfThreeWithTwentySevenSucceeds()
        {
            var number = 27;

            var expected = 3;
            var actual = FindRootOfThree.GetRootOfThree(number);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGetRootOfThreeReturnsCorrectResultForLargeInput()
        {
            var number = 2744;

            var expected = 14;
            var actual = FindRootOfThree.GetRootOfThree(number);

            Assert.AreEqual(expected, actual);
        }

        // test with the maximum possible perfect cube

        [TestMethod]
        public void TestGetRootOfThreeShouldThrowForNegativeNumbers()
        {
            var number = -2744;

            Assert.ThrowsException<ArgumentException>(() => FindRootOfThree.GetRootOfThree(number), "The cube root could not be found.");
        }

        [TestMethod]
        public void TestGetRootOfThreeReturnsCorrectResultForMaxPossibleCube()
        {
            var number = 2146689000;

            var expected = 1290;
            var actual = FindRootOfThree.GetRootOfThree(number);

            Assert.AreEqual(expected, actual);
        }
    }
}
