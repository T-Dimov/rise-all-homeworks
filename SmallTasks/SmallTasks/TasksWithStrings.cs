﻿using System.Collections.Immutable;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.Text;
using System.Linq;

namespace SmallTasks
{
    public class TasksWithStrings
    {
        // 1. Reverse a String String Reverse(String argument)

        public static string Reverse(string text)
        {
            var reversedString = string.Empty;

            for (int i = text.Length - 1; i >= 0; i--)
            {
                reversedString += text[i];
            }

            return reversedString;
        }

        // 2. Reverse each word in a String String ReverseEveryWord(String arg) ReverseEveryWord("What is this") => tahW si siht

        public static string ReverseEveryWord(String text)
        {
            var textArray = text.Split(" ").ToArray();
            var result = string.Empty;

            for (int i = 0; i < textArray.Length; i++)
            {
                result += new string(textArray[i].Reverse().ToArray()) + " ";
            }

            return result.TrimEnd();
        }

        // 3. Palindrome? bool IsPalindrome(String argument)

        public static bool IsPalindrome(String word)
        {
            var revercedWord = Reverse(word);

            return word == revercedWord;
        }

        // 4. Number a palindrome? bool IsPalindrome(int argument)

        public static bool IsPalindrome(int number)
        {
            var revercedNumberAsString = Reverse(number.ToString());

            return number.ToString() == revercedNumberAsString;
        }

        // 5. the largest palindrome number up to N - largest number < N that is a palindrom long GetLargestPalindrome(long N);

        public static long GetLargestPalindrome(long N)
        {
            for (long i = N; i >= 0; i--)
            {
                if (IsPalindrome(i.ToString()))
                {
                    return i;
                }
            }

            return 0;
        }

        // 6. Copy character k-times String CopyChars(String input, int k) CopyChars("nbsp;", 3) => "nbsp;nbsp;nbsp;"

        public static string CopyChars(string input, int k)
        {
            if (k == 0)
            {
                return input;
            }

            var stringBuilder = new StringBuilder();

            for (int i = 0; i < k; i++)
            {
                stringBuilder.Append(input);
            }

            return stringBuilder.ToString();
        }

        // 7. Return number of (non-overlapping) occurences int Mentions(String word, String text) Mentions("what", "whattfwahtfwhatawhathwatwhat") => 4*

        public static int Mentions(string word, string text)
        {
            var mentions = text.Split(word, StringSplitOptions.RemoveEmptyEntries).ToArray().Count();

            if (mentions == 1)
            {
                return 0;
            }

            return mentions + 1;
        }

        // 8. decode an URL, using the following rules %20=>' ' %3A=>':' %3D=>'?' %2F=>'/' String DecodeUrl(String input)


        // 9. sum all the numbers in a String, ignoring characters int SumOfNumbers(String input)

        public static int SumOfNumbers(String input)
        {
            var sum = 0;

            for (int i = 0; i < input.Length; i++)
            {
                if (Char.IsDigit(input[i]))
                {
                    sum += int.Parse(input[i].ToString());
                }
            }

            return sum;
        }

        // 10. Is anagram? bool Anagram(String A, String B)

        public static bool Anagram(String a, String b)
        {
            var firstSortedString = b.OrderBy(x => x).ToString();
            var secondSortedString = b.OrderBy(x => x).ToString();

            if (firstSortedString == secondSortedString)
            {
                return true;
            }

            return false;
        }
    }
}
