﻿using SmallTasks;

namespace TasksWithNumbersTests
{
    [TestClass]
    public class UnitTest2
    {
        // 1. Reverse a String String Reverse(String argument)

        [TestMethod]
        public void TestReverseShouldWorkProperly()
        {
            var text = "Rise";

            var expected = "esiR";
            var actual = TasksWithStrings.Reverse(text);

            Assert.AreEqual(expected, actual);
        }

        // 2. Reverse each word in a String String ReverseEveryWord(String arg) ReverseEveryWord("What is this") => tahW si siht

        [TestMethod]
        public void TestReverseEveryWordShouldWorkProperly() 
        {
            var text = "What is this";

            var expected = "tahW si siht";
            var actual = TasksWithStrings.ReverseEveryWord(text);

            Assert.AreEqual(expected, actual);
        }

        // 3. Palindrome? bool IsPalindrome(String argument)

        [TestMethod]
        public void TestIsPalindromeReturnCorrectResult() 
        {
            var word = "madam";

            var expected = true;
            var actual = TasksWithStrings.IsPalindrome(word);
             
            Assert.AreEqual(expected, actual);
        }

        // 4. Number a palindrome? bool IsPalindrome(int argument)

        [TestMethod]
        public void TestIsPalindromeShouldReturnTrueWithPalindromeNumber() 
        {
            var number = 12321;

            var result = TasksWithStrings.IsPalindrome(number);

            Assert.IsTrue(result);
        }


        [TestMethod]
        public void TestIsPalindromeShouldReturnFalseWithNonPalindromeNumber()
        {
            var number = 12345;

            var result = TasksWithStrings.IsPalindrome(number);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestIsPalindromeShouldReturnTrueWithSingleNumber()
        {
            var number = 2;

            var result = TasksWithStrings.IsPalindrome(number);

            Assert.IsTrue(result);
        }

        // 5. the largest palindrome number up to N - largest number < N that is a palindrom long GetLargestPalindrome(long N);

        [TestMethod]
        public void TestGetLargestPalindromeShouldReturnTheCorrectValue() 
        {
            var number = 10000000000L;

            var expected = 9999999999;
            var actual = TasksWithStrings.GetLargestPalindrome(number);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGetLargestPalindromeShouldReturnTheCorrectValueWithZero()
        {
            var number = 0;

            var expected = 0;
            var actual = TasksWithStrings.GetLargestPalindrome(number);

            Assert.AreEqual(expected, actual);
        }

        // 6. Copy character k-times String CopyChars(String input, int k) CopyChars("nbsp;", 3) => "nbsp;nbsp;nbsp;"

        [TestMethod]
        public void TestCopyCharsShouldWorkProperlyWithKZero() 
        {
            var input = "abv";
            var k = 0;

            var actual = TasksWithStrings.CopyChars(input, k);
            var expected = "abv";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestCopyCharsShouldWorkProperly()
        {
            var input = "abv";
            var k = 3;

            var actual = TasksWithStrings.CopyChars(input, k);
            var expected = "abvabvabv";

            Assert.AreEqual(expected, actual);
        }

        // 7. Return number of (non-overlapping) occurences int Mentions(String word, String text) Mentions("what", "whattfwahtfwhatawhathwatwhat") => 4*

        [TestMethod]
        public void TestCopyCharsShouldReturnCorrectValue() 
        {
            var word = "what";
            var text = "whattfwahtfwhatawhathwatwhat";

            var expected = 4;
            var actual = TasksWithStrings.Mentions(word, text);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestCopyCharsShouldReturnCorrectValueWithZeroOccurrences()
        {
            var word = "what";
            var text = "asjsl";

            var expected = 0;
            var actual = TasksWithStrings.Mentions(word, text);

            Assert.AreEqual(expected, actual);
        }

        // 9. sum all the numbers in a String, ignoring characters int SumOfNumbers(String input)

        [TestMethod]
        public void TestSumOfNumbersShouldReturnCorrectResult() 
        {
            var input = "stg6jsh6hsj9jja0j";

            var expected = 21;
            var actual = TasksWithStrings.SumOfNumbers(input);

            Assert.AreEqual(expected, actual);
        }

        // 10. Is anagram? bool Anagram(String A, String B)

        [TestMethod]
        public void TestAnagramShoulReturnCorrectResult() 
        {
            var a = "mama";
            var b = "amam";

            var result = TasksWithStrings.Anagram(a, b);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestAnagramShoulReturnCorrectFalseWithNonAnagrams()
        {
            var a = "mama";
            var b = "banan";

            var result = TasksWithStrings.Anagram(a, b);

            Assert.IsTrue(result);
        }
    }
}
